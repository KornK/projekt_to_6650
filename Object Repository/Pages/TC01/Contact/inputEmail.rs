<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Adres emailowy nadawcy</description>
   <name>inputEmail</name>
   <tag></tag>
   <elementGuidId>6885c9e5-6395-46c1-9356-86625495d755</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[id=&quot;email&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
