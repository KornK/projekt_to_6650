<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk przekierowywujący na stronę kontaktową.</description>
   <name>aContact</name>
   <tag></tag>
   <elementGuidId>e59bca08-c183-44ed-a108-b99164951ac1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@href = 'http://automationpractice.com/index.php?controller=contact' and @title = 'Contact us']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[id=&quot;contact-link&quot;]>a[title=&quot;Contact Us&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://automationpractice.com/index.php?controller=contact</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>Contact us</value>
   </webElementProperties>
</WebElementEntity>
