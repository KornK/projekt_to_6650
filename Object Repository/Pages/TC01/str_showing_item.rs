<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wykaz produktów na stronie</description>
   <name>str_showing_item</name>
   <tag></tag>
   <elementGuidId>cd0d4764-9b04-4902-8e46-c0773173b5ac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@class = 'product-count']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-count</value>
   </webElementProperties>
</WebElementEntity>
