<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk wyszukiwania</description>
   <name>btnSearch</name>
   <tag></tag>
   <elementGuidId>aeceb0d8-be43-477b-a1a5-c64e6b225bbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>&lt;button type=&quot;submit&quot; name=&quot;submit_search&quot; class=&quot;btn btn-default button-search&quot;>
			&lt;span>Search&lt;/span>
		&lt;/button></value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-default button-search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default button-search</value>
   </webElementProperties>
</WebElementEntity>
