<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wyznacza liczbę zakupowanych produktów</description>
   <name>inputQuantity</name>
   <tag></tag>
   <elementGuidId>1c18fb70-fb23-4f9f-a631-c57e3e51fb19</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[id=&quot;quantity_wanted&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
