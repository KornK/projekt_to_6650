<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Dodaje produkt do koszyka</description>
   <name>btnAddCart</name>
   <tag></tag>
   <elementGuidId>49369c30-0a23-4e90-88f4-e3218dff43bc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>p[id=&quot;add_to_cart&quot;]>button[type=&quot;submit&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
