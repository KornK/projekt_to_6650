<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Cena Produktu</description>
   <name>spn_our_price</name>
   <tag></tag>
   <elementGuidId>d1e142f9-6d07-4ebe-a33b-e45a440f0a58</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[@id = 'our_price_display']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>our_price_display</value>
   </webElementProperties>
</WebElementEntity>
