<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przejście do strony zapłaty za produkt</description>
   <name>aProductCheckout</name>
   <tag></tag>
   <elementGuidId>eb975307-1e25-4201-88c8-dfd7e9c7b582</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//span[(text() = 'Proceed to checkout' or . = 'Proceed to checkout')]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[class=&quot;btn btn-default button button-medium&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
