<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Link pozwalający na wysłanie strony produktu do znajomego.</description>
   <name>a_send_friend_button</name>
   <tag></tag>
   <elementGuidId>2faeb37a-bf60-44eb-9b55-efea0afbe640</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[@id = 'send_friend_button']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a[id=&quot;send_friend_button&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>send_friend_button</value>
   </webElementProperties>
</WebElementEntity>
