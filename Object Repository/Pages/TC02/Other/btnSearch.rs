<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk wyszukiwania</description>
   <name>btnSearch</name>
   <tag></tag>
   <elementGuidId>25aa569d-7878-4610-9d64-039a270eb4f6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@class = 'btn btn-default button-search']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>&lt;button type=&quot;submit&quot; name=&quot;submit_search&quot; class=&quot;btn btn-default button-search&quot;>
			&lt;span>Search&lt;/span>
		&lt;/button></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-default button-search</value>
   </webElementProperties>
</WebElementEntity>
