<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Przycisk wylogowania</description>
   <name>aLogout</name>
   <tag></tag>
   <elementGuidId>3e040581-2022-4ccb-9593-bb9e7ea678dd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div[class=&quot;header_user_info&quot;]>a[class=&quot;logout&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
