<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wpisanie hasła</description>
   <name>inputPassword</name>
   <tag></tag>
   <elementGuidId>1a8adee9-c795-4323-99a4-4f904ec98353</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id = 'passwd']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[id=&quot;passwd&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>passwd</value>
   </webElementProperties>
</WebElementEntity>
