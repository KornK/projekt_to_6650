<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description>Wpisanie adresu pocztowego</description>
   <name>inputEmail</name>
   <tag></tag>
   <elementGuidId>45d4713d-2651-4fc2-891f-103e23ffef4b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//input[@id = 'email']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>input[id=&quot;email&quot;]</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>email</value>
   </webElementProperties>
</WebElementEntity>
