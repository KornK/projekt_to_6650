import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Uruchomienie przeglądarki z adresesm strony
WebUI.openBrowser(rawUrl = GlobalVariable.url)
WebUI.maximizeWindow()

//Wyszukiwaanie i asercje
WebUI.setText(findTestObject('Pages/TC01/inputSearch'), GlobalVariable.search_dress)
WebUI.click(findTestObject('Pages/TC01/btnSearch'))

found_results_el = WebUI.getText(findTestObject('Object Repository/Pages/TC01/str_found_results'))
assert found_results == found_results_el

showing_item_el = WebUI.getText(findTestObject('Object Repository/Pages/TC01/str_showing_item'))
assert showing_item == showing_item_el

WebUI.setText(findTestObject('Pages/TC01/inputSearch'), GlobalVariable.search_blouse)
WebUI.click(findTestObject('Pages/TC01/btnSearch'))


for (def row = 1; row <= findTestData('DF_6650/TCN_6650_DATA').getRowNumbers(); row++)
{

	WebUI.setText(findTestObject('Pages/TC01/inputSearch'),
		findTestData('DF_6650/TCN_6650_DATA').getValue('search_input', row))

	WebUI.click(findTestObject('Pages/TC01/btnSearch'))
}


//Przejście na stronę kontaktową
WebUI.click(findTestObject('Pages/TC01/aContact'))

//Wypełnienie formularza na stronie kontaktowej
WebUI.selectOptionByValue(findTestObject('Pages/TC01/Contact/lstServiceContact'), '2', false)
WebUI.setText(findTestObject('Pages/TC01/Contact/inputEmail'), GlobalVariable.email)
WebUI.setText(findTestObject('Pages/TC01/Contact/inputContactOrderRef'), GlobalVariable.order_ref)
WebUI.setText(findTestObject('Pages/TC01/Contact/txtMessage'), GlobalVariable.message)

WebUI.click(findTestObject('Pages/TC01/Contact/btnSend'))

//Zamknięcie przeglądarki
WebUI.closeBrowser()

