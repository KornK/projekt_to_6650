import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

//Uruchomienie przeglądarki z adresesm strony
WebUI.openBrowser(rawUrl=GlobalVariable.url)
WebUI.maximizeWindow()

for (def row = 1; row <= findTestData('DF_6650/TCN_6650_DATA').getRowNumbers(); row++)
{

	WebUI.setText(findTestObject('Pages/TC02/Other/inputSearch'),
		findTestData('DF_6650/TCN_6650_DATA').getValue('search_input', row))

	WebUI.click(findTestObject('Pages/TC02/Other/btnSearch'))
}


WebUI.setText(findTestObject('Pages/TC02/Other/inputSearch'), GlobalVariable.search_tshirt)
WebUI.click(findTestObject('Pages/TC02/Other/btnSearch'))

WebUI.click(findTestObject('Pages/TC02/Other/lnkProduct'))


//Strona produktu
our_price_el = WebUI.getText(findTestObject('Pages/TC02/ProductPage/spn_our_price'))
assert our_price == our_price_el

send_friend_button_el = WebUI.getText(findTestObject('Pages/TC02/ProductPage/a_send_friend_button'))
assert send_friend_button == send_friend_button_el

WebUI.setText(findTestObject('Pages/TC02/ProductPage/inputQuantity'), GlobalVariable.quantity)
WebUI.click(findTestObject('Pages/TC02/ProductPage/btnAddCart'))
WebUI.click(findTestObject('Pages/TC02/ProductPage/aProductCheckout'))


//Strony zapłaty za produkt
//1
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/aCheckout1'))

//2
WebUI.setText(findTestObject('Pages/TC02/CheckoutPages/inputEmail'), GlobalVariable.email)
WebUI.setText(findTestObject('Pages/TC02/CheckoutPages/inputPassword'), GlobalVariable.password)
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/btnSubmitLogin'))

//3
WebUI.setText(findTestObject('Pages/TC02/CheckoutPages/txtComment'), GlobalVariable.comment)
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/btnCheckout3'))

//4
WebUI.check(findTestObject('Pages/TC02/CheckoutPages/inputTermsofService'))
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/btnCheckout4'))

//5
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/aCheque'))

//Confirm
WebUI.click(findTestObject('Pages/TC02/CheckoutPages/btnCheckout6'))


//Wylogowanie się
WebUI.click(findTestObject('Pages/TC02/Other/aLogout'))

//Zamknięcie przeglądarki
WebUI.closeBrowser()